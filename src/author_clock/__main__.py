#!/usr/bin/env python3

import datetime
import os
import piltextbox
import psycopg
import random
import re
import string
from PIL import Image
from inky.auto import auto
from piltextbox.textbox import TextBox
from psycopg import sql
from typing import Optional, TypedDict, List


class QuoteEntry(TypedDict):
    time_phrase: str
    quote: str
    book: Optional[str]
    author: Optional[str]


db_user = os.environ["db_user"]
db_pass = os.environ["db_pass"]
db_name = os.environ["db_name"]
db_url = os.environ["db_url"]
db_port = os.environ["db_port"]


inky_display = auto()
inky_display.set_border(inky_display.WHITE)

SPACING = 8
LIBERATION_LOCATION = "/usr/share/fonts/truetype/liberation/"
liberation_map = {
    "regular": LIBERATION_LOCATION + "LiberationSerif-Regular.ttf",
    "bold": LIBERATION_LOCATION + "LiberationSerif-Bold.ttf",
    "ital": LIBERATION_LOCATION + "LiberationSerif-Italic.ttf",
    "boldital": LIBERATION_LOCATION + "LiberationSerif-BoldItalic.ttf",
}


# piltextbox doesn't like when bold/italic tags are surrounded by punctuation
# So we expand the time phrase to include punctuation on either side
def add_punc(phrase: str, time: str) -> str:
    query = re.search(time, phrase)
    if query:
        new_phrase = time
        time_start, time_end = query.span()

        time_start = time_start - 1

        punc = set(string.punctuation)

        while time_start >= 0 and phrase[time_start] in punc:
            new_phrase = "{char}{new_phrase}".format(
                char=phrase[time_start], new_phrase=new_phrase
            )
            time_start = time_start - 1

        while time_end < len(phrase) and phrase[time_end] in punc:
            new_phrase = "{new_phrase}{char}".format(
                char=phrase[time_end], new_phrase=new_phrase
            )
            time_end = time_end + 1

        return new_phrase

    return time


def format_time_phrase(phrase: str, time: str) -> str:
    time_with_punc = add_punc(phrase=phrase, time=time)
    formatted_phrase = phrase.replace(
        time_with_punc, "<b>{time_with_punc}</b>".format(time_with_punc=time_with_punc)
    )

    # piltextbox wants spaces in between tags
    # so if the first or last character of the phrase is
    #  part of a tag we prepend a space
    if formatted_phrase.index("<") == 0:
        formatted_phrase = " {formatted_phrase}".format(
            formatted_phrase=formatted_phrase
        )

    if formatted_phrase.index(">") == len(formatted_phrase) - 1:
        formatted_phrase = "{formatted_phrase} ".format(
            formatted_phrase=formatted_phrase
        )

    return "<i>{formatted_phrase}</i>".format(formatted_phrase=formatted_phrase)


def format_author_credit(author: Optional[str], book: Optional[str]) -> str:
    phrase = "-"
    if book:
        phrase += " <b>{book}</b>".format(book=book)
    if author:
        phrase += " by {author}".format(author=author)
    return phrase


def create_textbox(
    formatted_text: str, size: tuple[int, int], font_size: int, spacing=SPACING
) -> TextBox:
    textbox = piltextbox.TextBox(
        size=size,
        typeface=liberation_map["regular"],
        font_size=font_size,
        spacing=spacing,
        margins=(SPACING * 2, SPACING, SPACING * 2, SPACING),
    )

    textbox.set_truetype_font(typeface=liberation_map["bold"], style="bold")
    textbox.set_truetype_font(typeface=liberation_map["ital"], style="ital")
    textbox.set_truetype_font(typeface=liberation_map["boldital"], style="boldital")
    textbox.write_paragraph(formatted_text, formatting=True)
    return textbox


def draw_phrase(
    phrase: str, time: str, book: Optional[str], author: Optional[str]
) -> None:
    img = Image.new("RGBA", (inky_display.WIDTH, inky_display.HEIGHT), "white")

    phrase_textbox = create_textbox(
        formatted_text=format_time_phrase(phrase, time),
        size=(inky_display.WIDTH, (inky_display.HEIGHT - 80)),
        font_size=48,
    )
    credit_textbox = create_textbox(
        formatted_text=format_author_credit(author, book),
        size=(inky_display.WIDTH, 80),
        font_size=28,
    )

    rendered_phrase = phrase_textbox.im
    rendered_credit = credit_textbox.im

    if rendered_phrase and rendered_credit:
        img.paste(rendered_phrase, (SPACING, SPACING))
        img.paste(rendered_credit, (SPACING, rendered_phrase.height))
        inky_display.set_image(img)
        inky_display.show()


def get_quotes(time: str) -> Optional[List[QuoteEntry]]:
    with psycopg.connect(
        "postgresql://{db_user}:{db_pass}@{db_url}:{db_port}/{db_name}?sslmode=require".format(
            db_user=db_user,
            db_pass=db_pass,
            db_name=db_name,
            db_url=db_url,
            db_port=db_port,
        )
    ) as conn:
        with conn.cursor() as cur:
            query = sql.SQL(
                """SELECT time_phrase,quote,book,author 
                    FROM quotes 
                    WHERE time_of_day = %s AND approved = true
                """
            )

            records = cur.execute(query, (time,)).fetchall()
            columns = cur.description
            if not columns:
                return None

            results: List[QuoteEntry] = []
            for row in records:
                row_dict: QuoteEntry = {
                    "quote": "",
                    "time_phrase": "",
                    "book": None,
                    "author": None,
                }
                for i, col in enumerate(columns):
                    row_dict[col.name] = row[i]
                results.append(row_dict)
            return results


def current_time() -> str:
    return datetime.datetime.now().strftime("%H:%M")


def pick_random_quote(data: List[QuoteEntry]) -> QuoteEntry:
    num_rows = len(data)
    randrow = random.randrange(start=0, stop=num_rows)
    return data[randrow]


def main() -> None:
    quotes = get_quotes(current_time())
    if quotes is not None and len(quotes) > 0:
        quote = pick_random_quote(quotes)
        draw_phrase(
            quote["quote"], quote["time_phrase"], quote["book"], quote["author"]
        )


if __name__ == "__main__":
    main()
